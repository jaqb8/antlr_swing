tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},declarations={$d});

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> declare(id={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr)  {globals.hasSymbol($i1.text)}? -> setVariable(id={$ID.text},value={$e2.st})
        | ID                        {globals.hasSymbol($ID.text)}? -> getVariable(id={$ID.text})
        | INT                                                      -> getInt(i={$INT.text})
    ;
    