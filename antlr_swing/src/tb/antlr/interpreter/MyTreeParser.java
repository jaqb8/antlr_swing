package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer subtract(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer multiply(Integer a, Integer b) {
		return a * b;
	}
	
	protected Integer divide(Integer a, Integer b) {
		if (b == 0) {
			throw new RuntimeException("Dividing by zero is not allowed.");
		}
		return a / b;
	}
	
	protected void addVariable(String variableName) {
		globalSymbols.newSymbol(variableName);
	}
	
	protected Integer setVariable(String variableName, Integer value) {
		globalSymbols.setSymbol(variableName, value);
		return value;
	}
	
	protected Integer getVariableValue(String variableName) {
		return globalSymbols.getSymbol(variableName);
	}
	
	protected void printVariableValue(Integer value) {
		System.out.println(value.toString());
	}
}
